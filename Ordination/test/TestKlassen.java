package test;

import static org.junit.Assert.assertEquals;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;

public class TestKlassen {
  
  Laegemiddel basisdataLaegemiddel1;
  LocalDate basisdataStartDag;
  LocalDate basisdataSlutDag;
  
  double dosis1, dosis2, dosis3, dosis4, dosisPN;
  
  @Before
  public void setup() throws Exception {
    basisdataLaegemiddel1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15,
        0.16, "Styk");
    basisdataStartDag = LocalDate.of(2020, 04, 01);
    basisdataSlutDag = LocalDate.of(2020, 05, 01);
    dosis1 = 0.001;
    dosis2 = 0.002;
    dosis3 = 0.003;
    dosis4 = 0.004;
    dosisPN = 2;
    
  }
  
//TC1 - Daglig fast -Constructor1
  @Test
  public void testDagligFastConstructorTC1() {
    DagligFast tc = new DagligFast(basisdataStartDag, basisdataSlutDag,
        basisdataLaegemiddel1, dosis1, dosis2, dosis3, dosis4);
    assertEquals(basisdataStartDag, tc.getStartDen());
    assertEquals(basisdataSlutDag, tc.getSlutDen());
    assertEquals("Acetylsalicylsyre", tc.getLaegemiddel().getNavn());
    Assert.assertEquals(dosis1, tc.getDoser()[0].getAntal());
    Assert.assertEquals(dosis2, tc.getDoser()[1].getAntal());
    Assert.assertEquals(dosis3, tc.getDoser()[2].getAntal());
    Assert.assertEquals(dosis4, tc.getDoser()[3].getAntal());
    
  }
  
  @Test(expected = NullPointerException.class)
  
  public void testDagligFastConstructorTC2() {
    DagligFast tc = new DagligFast(basisdataStartDag, basisdataSlutDag, null,
        dosis1, dosis2, dosis3, dosis4);
    assertEquals(basisdataStartDag, tc.getStartDen());
    assertEquals(basisdataSlutDag, tc.getSlutDen());
    assertEquals("Acetylsalicylsyre", tc.getLaegemiddel().getNavn());
    Assert.assertEquals(dosis1, tc.getDoser()[0].getAntal());
    Assert.assertEquals(dosis2, tc.getDoser()[1].getAntal());
    Assert.assertEquals(dosis3, tc.getDoser()[2].getAntal());
    Assert.assertEquals(dosis4, tc.getDoser()[3].getAntal());
    
  }
  
  @Test
  public void testDagligFastDoegnDosisTC3() {
    DagligFast tc = new DagligFast(basisdataStartDag, basisdataSlutDag,
        basisdataLaegemiddel1, 2, 2, 2, 2);
    assertEquals(8, tc.doegnDosis(), 0.0001);
    
  }
  
  @Test
  public void testDagligFastDoegnDosisTC4() {
    DagligFast tc = new DagligFast(basisdataStartDag, basisdataSlutDag,
        basisdataLaegemiddel1, 0, 2, 2, 2);
    assertEquals(6, tc.doegnDosis(), 0.0001);
    
  }
  
  @Test
  public void testDagligFastDoegnDosisTC5() {
    DagligFast tc = new DagligFast(basisdataStartDag, basisdataSlutDag,
        basisdataLaegemiddel1, 0, 0, 0, 0);
    assertEquals(0, tc.doegnDosis(), 0.0001);
    
  }
  
  @Test
  public void testDagligFastDoegnDosisTC6() {
    DagligFast tc = new DagligFast(basisdataStartDag, basisdataSlutDag,
        basisdataLaegemiddel1, -1, 2, 2, 2);
    assertEquals(6, tc.doegnDosis(), 0.0001);
    
  }
  
  @Test
  public void testDagligFastSamletDosisTC7() {
    DagligFast tc = new DagligFast(basisdataStartDag, basisdataSlutDag,
        basisdataLaegemiddel1, 2, 2, 2, 2);
    assertEquals(248, tc.samletDosis(), 0.0001);
    
  }
  
  @Test
  public void testDagligSkaevConstructorTC1() {
    LocalTime lt1 = LocalTime.of(12, 30);
    LocalTime[] tider = tider = new LocalTime[1];
    double[] doser = new double[1];
    tider[0] = lt1;
    doser[0] = 2.0;
    DagligSkaev tc = new DagligSkaev(basisdataStartDag, basisdataSlutDag,
        basisdataLaegemiddel1, tider, doser);
    assertEquals(basisdataStartDag, tc.getStartDen());
    assertEquals(basisdataSlutDag, tc.getSlutDen());
    assertEquals("Acetylsalicylsyre", tc.getLaegemiddel().getNavn());
    assertEquals(lt1, tc.getDoser().get(0).getTid());
    assertEquals(2, tc.getDoser().get(0).getAntal(), 0.001);
    
  }
  
  @Test(expected = NullPointerException.class)
  public void testDagligSkaevConstructorTC2() {
    LocalTime lt1 = LocalTime.of(12, 30);
    LocalTime[] tider = tider = new LocalTime[1];
    double[] doser = new double[1];
    tider[0] = lt1;
    doser[0] = 2.0;
    DagligSkaev tc = new DagligSkaev(basisdataStartDag, basisdataSlutDag, null,
        tider, doser);
    assertEquals(basisdataStartDag, tc.getStartDen());
    assertEquals(basisdataSlutDag, tc.getSlutDen());
    assertEquals("Acetylsalicylsyre", tc.getLaegemiddel().getNavn());
    assertEquals(lt1, tc.getDoser().get(0).getTid());
    assertEquals(2, tc.getDoser().get(0).getAntal(), 0.001);
    
  }
  
  //
  @Test(expected = DateTimeException.class)
  public void testDagligSkaevConstructorTC3() {
    LocalTime lt1 = LocalTime.of(24, 00);
    LocalTime[] tider = tider = new LocalTime[1];
    double[] doser = new double[1];
    tider[0] = lt1;
    doser[0] = 2.0;
    DagligSkaev tc = new DagligSkaev(basisdataStartDag, basisdataSlutDag,
        basisdataLaegemiddel1, tider, doser);
    assertEquals(basisdataStartDag, tc.getStartDen());
    assertEquals(basisdataSlutDag, tc.getSlutDen());
    assertEquals("Acetylsalicylsyre", tc.getLaegemiddel().getNavn());
    assertEquals(lt1, tc.getDoser().get(0).getTid());
    assertEquals(2, tc.getDoser().get(0).getAntal(), 0.001);
    
  }
  
  //
  @Test(expected = DateTimeException.class)
  public void testDagligSkaevConstructorTC4() {
    LocalTime lt1 = LocalTime.of(-12, 30);
    LocalTime[] tider = tider = new LocalTime[1];
    double[] doser = new double[1];
    tider[0] = lt1;
    doser[0] = 2.0;
    DagligSkaev tc = new DagligSkaev(basisdataStartDag, basisdataSlutDag,
        basisdataLaegemiddel1, tider, doser);
    assertEquals(basisdataStartDag, tc.getStartDen());
    assertEquals(basisdataSlutDag, tc.getSlutDen());
    assertEquals("Acetylsalicylsyre", tc.getLaegemiddel().getNavn());
    assertEquals(lt1, tc.getDoser().get(0).getTid());
    assertEquals(2, tc.getDoser().get(0).getAntal(), 0.001);
    
  }
  
  @Test
  public void testDagligSkaevConstructorTC5() {
    LocalTime lt1 = LocalTime.of(00, 00);
    LocalTime[] tider = tider = new LocalTime[1];
    double[] doser = new double[1];
    tider[0] = lt1;
    doser[0] = 2.0;
    DagligSkaev tc = new DagligSkaev(basisdataStartDag, basisdataSlutDag,
        basisdataLaegemiddel1, tider, doser);
    assertEquals(basisdataStartDag, tc.getStartDen());
    assertEquals(basisdataSlutDag, tc.getSlutDen());
    assertEquals("Acetylsalicylsyre", tc.getLaegemiddel().getNavn());
    assertEquals(lt1, tc.getDoser().get(0).getTid());
    assertEquals(2, tc.getDoser().get(0).getAntal(), 0.001);
    
  }
  
  @Test
  public void testDagligSkaevDoegnDosisTC6() {
    LocalTime lt1 = LocalTime.of(11, 00);
    LocalTime[] tider = tider = new LocalTime[1];
    double[] doser = new double[1];
    tider[0] = lt1;
    doser[0] = 4.0;
    DagligSkaev tc = new DagligSkaev(basisdataStartDag, basisdataSlutDag,
        basisdataLaegemiddel1, tider, doser);
    assertEquals(4, tc.doegnDosis(), 0.001);
  }
  
  @Test
  public void testDagligSkaevDoegnDosisTC7() {
    LocalTime lt1 = LocalTime.of(11, 00);
    LocalTime lt2 = LocalTime.of(12, 00);
    LocalTime lt3 = LocalTime.of(12, 00);
    LocalTime[] tider = tider = new LocalTime[3];
    double[] doser = new double[3];
    tider[0] = lt1;
    tider[1] = lt2;
    tider[2] = lt3;
    
    doser[0] = 4.0;
    doser[1] = 2.0;
    doser[2] = 8.0;
    DagligSkaev tc = new DagligSkaev(basisdataStartDag, basisdataSlutDag,
        basisdataLaegemiddel1, tider, doser);
    assertEquals(14, tc.doegnDosis(), 0.001);
  }
  
  @Test
  public void testDagligSkaevSamletDosisTC8() {
    LocalTime lt1 = LocalTime.of(11, 00);
    LocalTime lt2 = LocalTime.of(12, 00);
    LocalTime lt3 = LocalTime.of(12, 00);
    LocalTime[] tider = tider = new LocalTime[3];
    double[] doser = new double[3];
    tider[0] = lt1;
    tider[1] = lt2;
    tider[2] = lt3;
    
    doser[0] = 4.0;
    doser[1] = 2.0;
    doser[2] = 8.0;
    DagligSkaev tc = new DagligSkaev(basisdataStartDag, basisdataSlutDag,
        basisdataLaegemiddel1, tider, doser);
    assertEquals(434, tc.samletDosis(), 0.001);
  }
  
  @Test
  public void testPNConstructorTC1() {
    
    PN tc = new PN(basisdataStartDag, basisdataSlutDag, basisdataLaegemiddel1,
        dosisPN);
    assertEquals(basisdataStartDag, tc.getStartDen());
    assertEquals(basisdataSlutDag, tc.getSlutDen());
    assertEquals("Acetylsalicylsyre", tc.getLaegemiddel().getNavn());
    Assert.assertEquals(dosisPN, tc.getAntalEnheder());
    
  }
  
  @Test(expected = NullPointerException.class)
  public void testPNConstructorTC2() {
    
    PN tc = new PN(basisdataStartDag, basisdataSlutDag, null, dosisPN);
    assertEquals(basisdataStartDag, tc.getStartDen());
    assertEquals(basisdataSlutDag, tc.getSlutDen());
    assertEquals("Acetylsalicylsyre", tc.getLaegemiddel().getNavn());
    Assert.assertEquals(dosisPN, tc.getAntalEnheder());
    
  }
  
  @Test
  public void testPNGivDosisTC3() {
    PN tc = new PN(basisdataStartDag, basisdataSlutDag, basisdataLaegemiddel1,
        dosisPN);
    LocalDate ld1 = LocalDate.of(2020, 04, 20);
    assertEquals(true, tc.givDosis(ld1));
  }
  
  @Test
  public void testPNGivDosisTC4() {
    PN tc = new PN(basisdataStartDag, basisdataSlutDag, basisdataLaegemiddel1,
        dosisPN);
    LocalDate ld1 = LocalDate.of(2020, 03, 31);
    assertEquals(false, tc.givDosis(ld1));
  }
  
  @Test
  public void testPNGivDosisTC5() {
    PN tc = new PN(basisdataStartDag, basisdataSlutDag, basisdataLaegemiddel1,
        dosisPN);
    LocalDate ld1 = LocalDate.of(2020, 05, 02);
    assertEquals(false, tc.givDosis(ld1));
  }
  
  @Test
  public void testPNGivDosisTC6() {
    PN tc = new PN(basisdataStartDag, basisdataSlutDag, basisdataLaegemiddel1,
        dosisPN);
    LocalDate ld1 = LocalDate.of(2020, 04, 01);
    assertEquals(true, tc.givDosis(ld1));
  }
  
  @Test
  public void testPNGivDosisTC7() {
    PN tc = new PN(basisdataStartDag, basisdataSlutDag, basisdataLaegemiddel1,
        dosisPN);
    LocalDate ld1 = LocalDate.of(2020, 05, 01);
    assertEquals(true, tc.givDosis(ld1));
  }
  
  @Test
  public void testPNDoegnDosisTC8() {
    PN tc = new PN(basisdataStartDag, basisdataSlutDag, basisdataLaegemiddel1,
        dosisPN);
    LocalDate ld1 = LocalDate.of(2020, 4, 20);
    tc.givDosis(ld1);
    tc.givDosis(ld1);
    assertEquals(4, tc.doegnDosis(), 0.001);
  }
  
  @Test
  public void testPNDoegnDosisTC9() {
    PN tc = new PN(basisdataStartDag, basisdataSlutDag, basisdataLaegemiddel1,
        dosisPN);
    LocalDate ld1 = LocalDate.of(2020, 4, 24);
    tc.givDosis(ld1);
    tc.givDosis(ld1);
    tc.givDosis(ld1);
    tc.givDosis(ld1);
    tc.givDosis(ld1);
    tc.givDosis(ld1);
    tc.givDosis(ld1);
    assertEquals(14, tc.doegnDosis(), 0.001);
  }
  
  @Test
  public void testPNDoegnDosisTC10() {
    PN tc = new PN(basisdataStartDag, basisdataSlutDag, basisdataLaegemiddel1,
        dosisPN);
    LocalDate ld1 = LocalDate.of(2020, 4, 2);
    LocalDate ld2 = LocalDate.of(2020, 4, 11);
    LocalDate ld3 = LocalDate.of(2020, 4, 12);
    LocalDate ld4 = LocalDate.of(2020, 4, 28);
    tc.givDosis(ld1);
    tc.givDosis(ld2);
    tc.givDosis(ld3);
    tc.givDosis(ld3);
    tc.givDosis(ld4);
    
    assertEquals(0.37037, tc.samletDosis(), 0.001);
  }
  
}
