package test;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import junit.framework.Assert;
import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class TestController {
  Patient basisdataPatient1;
  Laegemiddel basisdataLaegemiddel1;
  LocalDate basisdataStartDag;
  LocalDate basisdataSlutDag;
  Controller controller;
  
  double dosis1, dosis2, dosis3, dosis4, dosisPN;
  
  @Before
  public void setup() throws Exception {
    controller = Controller.getController();
    basisdataPatient1 = new Patient("121256-0512", "Jane Jensen", 63.4);
    basisdataLaegemiddel1 = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15,
        0.16, "Styk");
    basisdataStartDag = LocalDate.of(2020, 04, 01);
    basisdataSlutDag = LocalDate.of(2020, 05, 01);
    dosis1 = 0.001;
    dosis2 = 0.002;
    dosis3 = 0.003;
    dosis4 = 0.004;
    dosisPN = 2;
    
  }
  
  @Test
  public void testDagligFastTC1() {
    DagligFast tc = controller.opretDagligFastOrdination(basisdataStartDag,
        basisdataSlutDag, basisdataPatient1, basisdataLaegemiddel1, dosis1,
        dosis2, dosis3, dosis4);
    assertEquals(basisdataStartDag, tc.getStartDen());
    assertEquals(basisdataSlutDag, tc.getSlutDen());
    assertEquals("Acetylsalicylsyre", tc.getLaegemiddel().getNavn());
    Assert.assertEquals(dosis1, tc.getDoser()[0].getAntal());
    Assert.assertEquals(dosis2, tc.getDoser()[1].getAntal());
    Assert.assertEquals(dosis3, tc.getDoser()[2].getAntal());
    Assert.assertEquals(dosis4, tc.getDoser()[3].getAntal());
    assertEquals(tc, basisdataPatient1.getOrdinationer().get(0));
    
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void testDagligFastTC2() {
    DagligFast tc = controller.opretDagligFastOrdination(basisdataSlutDag,
        basisdataStartDag, basisdataPatient1, basisdataLaegemiddel1, dosis1,
        dosis2, dosis3, dosis4);
    assertEquals(basisdataStartDag, tc.getStartDen());
    assertEquals(basisdataSlutDag, tc.getSlutDen());
    assertEquals("Acetylsalicylsyre", tc.getLaegemiddel().getNavn());
    Assert.assertEquals(dosis1, tc.getDoser()[0].getAntal());
    Assert.assertEquals(dosis2, tc.getDoser()[1].getAntal());
    Assert.assertEquals(dosis3, tc.getDoser()[2].getAntal());
    Assert.assertEquals(dosis4, tc.getDoser()[3].getAntal());
    
  }
  
  @Test
  public void testAnbefaletDosisPerDoegnTC1() {
    basisdataPatient1 = new Patient("121256-0512", "Jane Jensen", 20);
    assertEquals(0.1, controller.anbefaletDosisPrDoegn(basisdataPatient1,
        basisdataLaegemiddel1), 0.001);
  }
  
  @Test
  public void testAnbefaletDosisPerDoegnTC2() {
    basisdataPatient1 = new Patient("121256-0512", "Jane Jensen", 25);
    assertEquals(0.15, controller.anbefaletDosisPrDoegn(basisdataPatient1,
        basisdataLaegemiddel1), 0.001);
  }
  
  @Test
  public void testAnbefaletDosisPerDoegnTC3() {
    basisdataPatient1 = new Patient("121256-0512", "Jane Jensen", 50);
    assertEquals(0.15, controller.anbefaletDosisPrDoegn(basisdataPatient1,
        basisdataLaegemiddel1), 0.001);
  }
  
  @Test
  public void testAnbefaletDosisPerDoegnTC4() {
    basisdataPatient1 = new Patient("121256-0512", "Jane Jensen", 125);
    assertEquals(0.15, controller.anbefaletDosisPrDoegn(basisdataPatient1,
        basisdataLaegemiddel1), 0.001);
  }
  
  @Test
  public void testAnbefaletDosisPerDoegnTC5() {
    basisdataPatient1 = new Patient("121256-0512", "Jane Jensen", 150);
    assertEquals(0.16, controller.anbefaletDosisPrDoegn(basisdataPatient1,
        basisdataLaegemiddel1), 0.001);
  }
  
  @Test
  public void antalOrdinationerPrVægtPrLaegemiddelTC1() {
    
    Patient p0 = controller.opretPatient("121256-0512", "Jane Jensen", 63.4);
    Patient p1 = controller.opretPatient("070985-1153", "Finn Madsen", -4);
    Patient p2 = controller.opretPatient("050972-1233", "Hans Jørgensen", 89.4);
    Patient p3 = controller.opretPatient("011064-1522", "Ulla Nielsen", 59.9);
    Patient p4 = controller.opretPatient("090149-2529", "Ib Hansen", 87.7);
    
    Laegemiddel l0 = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15,
        0.16, "Styk");
    Laegemiddel l1 = controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2,
        "Ml");
    Laegemiddel l2 = controller.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025,
        "Styk");
    Laegemiddel l3 = controller.opretLaegemiddel("Methotrexat", 0.01, 0.015,
        0.02, "Styk");
    
    controller.opretPNOrdination(LocalDate.of(2019, 1, 1),
        LocalDate.of(2019, 1, 12), p0, l1, 123);
    
    controller.opretPNOrdination(LocalDate.of(2019, 2, 12),
        LocalDate.of(2019, 2, 14), p0, l0, 3);
    
    controller.opretPNOrdination(LocalDate.of(2019, 1, 20),
        LocalDate.of(2019, 1, 25), p3, l2, 5);
    
    controller.opretDagligFastOrdination(LocalDate.of(2019, 1, 10),
        LocalDate.of(2019, 1, 12), p1, l1, 2, -1, 1, -1);
    
    LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
        LocalTime.of(16, 0), LocalTime.of(18, 45) };
    double[] an = { 0.5, 1, 2.5, 3 };
    
    controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23),
        LocalDate.of(2019, 1, 24), p1, l2, kl, an);
    
    assertEquals(1, controller.antalOrdinationerPrVægtPrLægemiddel(50, 100, l1),
        0.001);
  }
  
  @Test
  public void antalOrdinationerPrVægtPrLaegemiddelTC2() {
    
    Patient p0 = controller.opretPatient("121256-0512", "Jane Jensen", 63.4);
    Patient p1 = controller.opretPatient("070985-1153", "Finn Madsen", 83.2);
    Patient p2 = controller.opretPatient("050972-1233", "Hans Jørgensen", 89.4);
    Patient p3 = controller.opretPatient("011064-1522", "Ulla Nielsen", 59.9);
    Patient p4 = controller.opretPatient("090149-2529", "Ib Hansen", 87.7);
    
    Laegemiddel l0 = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15,
        0.16, "Styk");
    Laegemiddel l1 = controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2,
        "Ml");
    Laegemiddel l2 = controller.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025,
        "Styk");
    Laegemiddel l3 = controller.opretLaegemiddel("Methotrexat", 0.01, 0.015,
        0.02, "Styk");
    
    controller.opretPNOrdination(LocalDate.of(2019, 1, 1),
        LocalDate.of(2019, 1, 12), p0, l1, 123);
    
    controller.opretPNOrdination(LocalDate.of(2019, 2, 12),
        LocalDate.of(2019, 2, 14), p0, l0, 3);
    
    controller.opretPNOrdination(LocalDate.of(2019, 1, 20),
        LocalDate.of(2019, 1, 25), p3, l2, 5);
    
    controller.opretDagligFastOrdination(LocalDate.of(2019, 1, 10),
        LocalDate.of(2019, 1, 12), p1, l1, 2, -1, 1, -1);
    
    LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
        LocalTime.of(16, 0), LocalTime.of(18, 45) };
    double[] an = { 0.5, 1, 2.5, 3 };
    
    controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23),
        LocalDate.of(2019, 1, 24), p1, l2, kl, an);
    
    assertEquals(2, controller.antalOrdinationerPrVægtPrLægemiddel(50, 100, l1),
        0.001);
  }
  
  @Test
  public void antalOrdinationerPrVægtPrLaegemiddelTC3() {
    
    Patient p0 = controller.opretPatient("121256-0512", "Jane Jensen", 63.4);
    Patient p1 = controller.opretPatient("070985-1153", "Finn Madsen", 150);
    Patient p2 = controller.opretPatient("050972-1233", "Hans Jørgensen", 89.4);
    Patient p3 = controller.opretPatient("011064-1522", "Ulla Nielsen", 59.9);
    Patient p4 = controller.opretPatient("090149-2529", "Ib Hansen", 87.7);
    
    Laegemiddel l0 = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15,
        0.16, "Styk");
    Laegemiddel l1 = controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2,
        "Ml");
    Laegemiddel l2 = controller.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025,
        "Styk");
    Laegemiddel l3 = controller.opretLaegemiddel("Methotrexat", 0.01, 0.015,
        0.02, "Styk");
    
    controller.opretPNOrdination(LocalDate.of(2019, 1, 1),
        LocalDate.of(2019, 1, 12), p0, l1, 123);
    
    controller.opretPNOrdination(LocalDate.of(2019, 2, 12),
        LocalDate.of(2019, 2, 14), p0, l0, 3);
    
    controller.opretPNOrdination(LocalDate.of(2019, 1, 20),
        LocalDate.of(2019, 1, 25), p3, l2, 5);
    
    controller.opretDagligFastOrdination(LocalDate.of(2019, 1, 10),
        LocalDate.of(2019, 1, 12), p1, l1, 2, -1, 1, -1);
    
    LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
        LocalTime.of(16, 0), LocalTime.of(18, 45) };
    double[] an = { 0.5, 1, 2.5, 3 };
    
    controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23),
        LocalDate.of(2019, 1, 24), p1, l2, kl, an);
    
    assertEquals(1, controller.antalOrdinationerPrVægtPrLægemiddel(50, 100, l1),
        0.001);
  }
  
  @Test
  public void antalOrdinationerPrVægtPrLaegemiddelTC4() {
    
    Patient p0 = controller.opretPatient("121256-0512", "Jane Jensen", 63.4);
    Patient p1 = controller.opretPatient("070985-1153", "Finn Madsen", 83.2);
    Patient p2 = controller.opretPatient("050972-1233", "Hans Jørgensen", 89.4);
    Patient p3 = controller.opretPatient("011064-1522", "Ulla Nielsen", 59.9);
    Patient p4 = controller.opretPatient("090149-2529", "Ib Hansen", 87.7);
    
    Laegemiddel l0 = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15,
        0.16, "Styk");
    Laegemiddel l1 = controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2,
        "Ml");
    Laegemiddel l2 = controller.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025,
        "Styk");
    Laegemiddel l3 = controller.opretLaegemiddel("Methotrexat", 0.01, 0.015,
        0.02, "Styk");
    
    controller.opretPNOrdination(LocalDate.of(2019, 1, 1),
        LocalDate.of(2019, 1, 12), p0, l1, 123);
    
    controller.opretPNOrdination(LocalDate.of(2019, 2, 12),
        LocalDate.of(2019, 2, 14), p0, l0, 3);
    
    controller.opretPNOrdination(LocalDate.of(2019, 1, 20),
        LocalDate.of(2019, 1, 25), p3, l2, 5);
    
    controller.opretDagligFastOrdination(LocalDate.of(2019, 1, 10),
        LocalDate.of(2019, 1, 12), p1, l1, 2, -1, 1, -1);
    
    LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
        LocalTime.of(16, 0), LocalTime.of(18, 45) };
    double[] an = { 0.5, 1, 2.5, 3 };
    
    controller.opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23),
        LocalDate.of(2019, 1, 24), p1, l2, kl, an);
    
    assertEquals(2, controller.antalOrdinationerPrVægtPrLægemiddel(50, 100, l2),
        0.001);
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void ordinationPnAnvendtTC1() {
    LocalDate ld1 = LocalDate.of(2020, 02, 28);
    PN tc = controller.opretPNOrdination(basisdataStartDag, basisdataSlutDag,
        basisdataPatient1, basisdataLaegemiddel1, dosisPN);
    controller.ordinationPNAnvendt(tc, ld1);
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void ordinationPnAnvendtTC2() {
    LocalDate ld1 = LocalDate.of(2020, 05, 02);
    PN tc = controller.opretPNOrdination(basisdataStartDag, basisdataSlutDag,
        basisdataPatient1, basisdataLaegemiddel1, dosisPN);
    controller.ordinationPNAnvendt(tc, ld1);
  }
  
  @Test
  public void ordinationPnAnvendtTC3() {
    LocalDate ld1 = LocalDate.of(2020, 04, 01);
    PN tc = controller.opretPNOrdination(basisdataStartDag, basisdataSlutDag,
        basisdataPatient1, basisdataLaegemiddel1, dosisPN);
    controller.ordinationPNAnvendt(tc, ld1);
  }
  
  @Test
  public void ordinationPnAnvendtTC4() {
    LocalDate ld1 = LocalDate.of(2020, 04, 12);
    PN tc = controller.opretPNOrdination(basisdataStartDag, basisdataSlutDag,
        basisdataPatient1, basisdataLaegemiddel1, dosisPN);
    controller.ordinationPNAnvendt(tc, ld1);
  }
}
