package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {

 private Dosis[] doser = new Dosis[4];

 /**
  * 
  * @param startDen
  * @param slutDen
  * @param laegemiddel
  * @param morgenAntal / Vi har antaget at morgen er kl 08:00
  * @param middagAntal / Vi har antaget at middag er kl 12:00
  * @param aftenAntal  / Vi har antaget at aften er kl 17:00
  * @param natAntal    / Vi har antaget at nat er kl 21:00
  */
 public DagligFast(LocalDate startDen, LocalDate slutDen,
   Laegemiddel laegemiddel, double morgenAntal, double middagAntal,
   double aftenAntal, double natAntal) {
  super(startDen, slutDen, laegemiddel);
  double[] antal = { morgenAntal, middagAntal, aftenAntal, natAntal };
  LocalTime[] tidspunkter = { LocalTime.of(8, 0), LocalTime.of(12, 0), LocalTime.of(17, 0), LocalTime.of(21, 0) };

  for (int i = 0; i < 4; i++) {
   double j = antal[i];

   if (j < 0) {
    j = 0;
   }
   doser[i] = new Dosis(tidspunkter[i], j);
  }
 }

 @Override
 public LocalDate getStartDen() {
  return super.getStartDen();
 }

 @Override
 public LocalDate getSlutDen() {
  return super.getSlutDen();
 }

 public Dosis[] getDoser() {
  return doser.clone();
 }

 /**
  * Antal hele dage mellem startdato og slutdato. Begge dage inklusive.
  *
  * @return antal dage ordinationen gælder for
  */

 @Override
 public double samletDosis() {
  return doegnDosis() * antalDage();
 }

 /**
  * Returnerer den gennemsnitlige dosis givet pr dag i den periode ordinationen
  * er gyldig
  * 
  * @return
  */

 @Override
 public double doegnDosis() {
  double total = 0;
  for (Dosis d : doser) {
   total += d.getAntal();
  }
  return total;
 }

 /**
  * Returnerer ordinationstypen som en String
  * 
  * @return
  */

 @Override
 public String getType() {
  return "Daglig fast";
 }

 @Override
 public String toString() {
  int i = 0;
  for (Dosis d : doser) {
   if (d.getAntal() > 0) {
    i++;
   }
  }
  return getLaegemiddel() + " gives " + i + " gange i døgnet, i " + super.antalDage() + " dage, med start "
    + super.toString();
 }
}
