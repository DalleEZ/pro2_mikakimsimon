package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	// TODO
	ArrayList<Dosis> doser = new ArrayList<Dosis>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, LocalTime[] klokkeSlet,
			double[] antalEnheder) {
		super(startDen, slutDen, laegemiddel);
		for (int i = 0; i < klokkeSlet.length; i++) {
			opretDosis(klokkeSlet[i], antalEnheder[i]);
		}

	}

	public void opretDosis(LocalTime tid, double antal) {
		// TODO
		Dosis dosisa = new Dosis(tid, antal);
		doser.add(dosisa);
	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<Dosis>(doser);
	}

	/**
	 * Returnerer den totale dosis der er givet i den periode ordinationen er gyldig
	 * 
	 * @return
	 */
	@Override
	public double samletDosis() {
		// TODO Auto-generated method stub
//		double dagligDosis = 0;
//		long antalDage = getStartDen().until(getSlutDen(), ChronoUnit.DAYS);
//		for (Dosis d : doser) {
//			dagligDosis += d.getAntal();
//		}
//
//		return dagligDosis * antalDage;
		return doegnDosis() * antalDage();
	}

	/**
	 * Returnerer den gennemsnitlige dosis givet pr dag i den periode ordinationen
	 * er gyldig
	 * 
	 * @return
	 */
	@Override
	public double doegnDosis() {
		// TODO Auto-generated method stub

		double samledeDosis = 0;
		for (Dosis d : doser) {
			samledeDosis += d.getAntal();
		}
		return samledeDosis;
	}

	/**
	 * Returnerer ordinationstypen som en String
	 * 
	 * @return
	 */

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return "Dagligskaev Dosis";
	}

	@Override
	public String toString() {
		int i = 0;
		for (Dosis d : doser) {
			if (d.getAntal() > 0) {
				i++;
			}
		}
		return getLaegemiddel() + " gives " + i + " gange i døgnet, i " + super.antalDage() + " dage, med start "
				+ super.toString();
	}
}
