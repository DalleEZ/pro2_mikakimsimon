package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public abstract class Ordination {
  private LocalDate startDen;
  private LocalDate slutDen;
  // TODO Link til Laegemiddel
  private Laegemiddel laegemiddel;
  
  // TODO constructor (med specifikation)
  /**
   * Opretter en ordination med startdato, slutdato<br>
   * og lægemiddel pre: startDato er før slutDato, og lægemiddel er ikke null
   * 
   * @param startDen
   * @param slutDen
   * @param laegemiddel
   */
  public Ordination(LocalDate startDen, LocalDate slutDen,
      Laegemiddel laegemiddel) {
    this.startDen = startDen;
    this.slutDen = slutDen;
    this.laegemiddel = laegemiddel;
  }
  
  public Laegemiddel getLaegemiddel() {
    return this.laegemiddel;
  }
  
  public LocalDate getStartDen() {
    return this.startDen;
  }
  
  public LocalDate getSlutDen() {
    return this.slutDen;
  }
  
  /**
   * Antal hele dage mellem startdato og slutdato. <br>
   * Begge dage inklusive.
   * 
   * @return antal dage ordinationen gælder for
   */
  public int antalDage() {
    return (int) ChronoUnit.DAYS.between(startDen, slutDen)+1;
  }
  
  @Override
  public String toString() {
    return startDen.toString();
  }
  
  /**
   * Returnerer den totale dosis der er givet i den periode <br>
   * ordinationen er gyldig
   * 
   * @return
   */
  public abstract double samletDosis();
  
  /**
   * Returnerer den gennemsnitlige dosis givet pr dag i den periode ordinationen
   * er gyldig
   * 
   * @return
   */
  public abstract double doegnDosis();
  
  /**
   * Returnerer ordinationstypen som en String
   * 
   * @return
   */
  public abstract String getType();
}
