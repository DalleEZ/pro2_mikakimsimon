package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination {
  
  private double antalEnheder;
  private ArrayList<LocalDate> dosisGivet = new ArrayList<>();
  
  public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel,
      double antal) {
    super(startDen, slutDen, laegemiddel);
    this.antalEnheder = antal;
  }
  
  public Laegemiddel getLaegemiddel() {
    return super.getLaegemiddel();
  }
  
  /**
   * Registrerer at der er givet en dosis paa dagen<br>
   * givesDen returnerer true hvis givesDen er inden for ordinationens
   * gyldighedsperiode og datoen huskes<br>
   * Returner false ellers og datoen givesDen ignoreres<br>
   * Der sikres at Arraylisten forbliver kronologisk
   * 
   * @param givesDen
   * @return
   */
  public boolean givDosis(LocalDate givesDen) {
    if (givesDen.isAfter(getStartDen().minusDays(1))// Der må gives den første
        // dag
        && givesDen.isBefore(getSlutDen().plusDays(1))) {// Den sidste dag er
      // inkl.
      
      int dosisInd = dosisGivet.size()-1;
      while (dosisInd >= 0) {
        if (givesDen.compareTo(dosisGivet.get(dosisInd)) >= 0) {
          break;
        }
        dosisInd--;
      }
      dosisGivet.add(dosisInd+1, givesDen);
      return true;
    }
    else {
      return false;
    }
  }
  
  /**
   * Dosis per doegn, her beregnet som et gennemsnit<br>
   * af dosiser ialt delt med dage i intervallet mellem<br>
   * første og sidste dag, hvor der er givet. <br>
   */
  public double doegnDosis() {
    if (dosisGivet.isEmpty()) {
      return 0;
    }
    
    return dosisGivet.size()*this.antalEnheder;
  }
  
  /**
   * Returner den daglige dosis multipliceret med antal gange,<br>
   * der er givet en dosis
   */
  public double samletDosis() {
    // ArrayListen er sorteret kronologisk, derfor blot første og sidste
    long interval = dosisGivet.get(0).until(dosisGivet.get(dosisGivet.size()-1),
        ChronoUnit.DAYS);
    interval++;// stakit princippet
    return (this.antalEnheder*this.dosisGivet.size()/interval);
  }
  
  /**
   * Returnerer antal gange ordinationen er anvendt
   * 
   * @return
   */
  public int getAntalGangeGivet() {
    
    return dosisGivet.size();
  }
  
  public double getAntalEnheder() {
    return antalEnheder;
  }
  
  @Override
  public String getType() {
    // TODO Auto-generated method stub
    return "PN";
  }
  
  @Override
  public String toString() {
    
    return super.getLaegemiddel() + " gives efter behov i " + super.antalDage()
        + " dage, med start " + super.getStartDen();
  }
}
